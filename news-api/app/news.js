const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },

  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {
  router.get('/', (req, res) => {
    db.query('SELECT * FROM `news`', function (error, results) {
      if (error) throw error;

      res.send(results);
    });
  });

  router.post('/', upload.single('image'), (req, res) => {
    const news = req.body;

    if (req.file) {
      news.image = req.file.filename;
    } else {
      news.image = null;
    }

    db.query(
      'INSERT INTO `news` (`headline`, `content`) ' +
      'VALUES (?, ?)',
      [1, news.content],
      (error, results) => {
        if (error) throw error;

        news.id = results.insertId;
        res.send(news);
      }
    );
  });

  router.get('/:id', (req, res) => {
    res.send(db.getDataById(req.params.id));

  });

  router.delete('/:id', (req, res) => {
      db()
  });

  return router;
};

module.exports = createRouter;