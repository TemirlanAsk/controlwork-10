const express = require('express');
const cors = require('cors');
const mysql      = require('mysql');

const news = require('./app/news');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'news'
});

connection.connect((err) => {
  if (err) throw err;

  app.use('/news', news(connection));

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

});