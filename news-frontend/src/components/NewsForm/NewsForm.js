import React, {Component} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";

class NewForm extends Component {
  state = {
    title: '',
    content: ''

  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  // fileChangeHandler = event => {
  //   this.setState({
  //     [event.target.name]: event.target.files[0]
  //   });
  // };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        <FormGroup controlId="NewTitle">
          <Col componentClass={ControlLabel} sm={2}>
            Title
          </Col>
          <Col sm={10}>
            <FormControl
              type="text" required
              placeholder="Enter New title"
              name="title"
              value={this.state.title}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="NewDescription">
          <Col componentClass={ControlLabel} sm={2} >
            Content
          </Col>
          <Col sm={10}>
            <FormControl
              componentClass="textarea"
              placeholder="Enter your comment"
              name="content"
              value={this.state.content}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>
        {/*<FormGroup controlId="NewImage">*/}
          {/*<Col componentClass={ControlLabel} sm={2}>*/}
            {/*Image*/}
          {/*</Col>*/}
          {/*<Col sm={10}>*/}
            {/*<FormControl*/}
              {/*type="file"*/}
              {/*name="image"*/}
              {/*onChange={this.fileChangeHandler}*/}
            {/*/>*/}
          {/*</Col>*/}
        {/*</FormGroup>*/}

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default NewForm;
