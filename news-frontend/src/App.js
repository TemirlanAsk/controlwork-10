import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";

import Toolbar from './components/UI/Toolbar/Toolbar';
import News from "./containers/News/News";
import NewNews from "./containers/NewNews/NewNews";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header><Toolbar/></header>
        <main className="container">
          <Switch>
            <Route path="/" exact component={News} />
            <Route path="/news/new" exact component={NewNews} />
          </Switch>
        </main>
      </Fragment>
    );
  }
}

export default App;
