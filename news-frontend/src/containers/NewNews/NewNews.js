import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import NewsForm from "../../components/NewsForm/NewsForm";
import {createNews} from "../../store/actions/news";

class NewNews extends Component {
  createNews = newsData => {
    this.props.onNewsCreated(newsData).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <Fragment>
        <PageHeader>News</PageHeader>
        <NewsForm onSubmit={this.createNews} />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onNewsCreated: newsData => {
      return dispatch(createNews(newsData))
    }
  }
};

export default connect(null, mapDispatchToProps)(NewNews);