import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Image, PageHeader, Panel} from "react-bootstrap";
import {fetchNews} from "../../store/actions/news";
import {Link} from "react-router-dom";

class News extends Component {
  componentDidMount() {
    this.props.onFetchNews();
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Posts
          <Link to="/news/new">
            <Button bsStyle="primary" className="pull-right">
              Add New Post
            </Button>
          </Link>
        </PageHeader>

        {this.props.news.map(res => (
          <Panel key={res.id} style={{'height': '80px', 'background': '#bbe6c566'}}>
              <Button bsStyle="danger" type="submit" className="pull-right" style={{'margin': '20px'}} >Delete</Button>
            <div style={{'margin': '25px'}}>{res.content}</div>
          </Panel>
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    news: state.news.news
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchNews: () => dispatch(fetchNews())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(News);